﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace My_CodeFirst_ASP.NET_MVC.Models
{
    public class Student
    {
        [Key]
        public int StudentID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}