﻿using ProductApps.Models;
using System.Data.Entity;

namespace ProductApps.Context
{
    public class ProductContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}