﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyDBFirstApplications.Startup))]
namespace MyDBFirstApplications
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
