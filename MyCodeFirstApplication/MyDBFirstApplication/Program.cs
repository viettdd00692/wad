﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyDBFirstApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //---Them moi sinh vien
            //using (var context = new FptDBFirstEntities2())
            //{
            //    var std = new Student()
            //    {
            //        StudentName = "Khanh Toan"
            //    };
            //    context.Students.Add(std);
            //    context.SaveChanges();
            //}

            //---LINQ Query
            //using (var context = new FptDBFirstEntities2())
            //{
                //var student = (from s in context.Students where s.StudentName == "Khanh Toan" select s).FirstOrDefault<Student>();

                //---Lambda Expression vs LINQ
                //string name = "Khanh Toan";
                //var student1 = context.Students.Where(s => s.StudentName == name).FirstOrDefault<Student>();

                //---Return list of student
                //var studentList = context.Students.Where(s => s.StudentName == "Khanh Toan").ToList();
            //}

            //---GroupBy
            using (var ctx = new FptDBFirstEntities2())
            {
                //---LINQ standard
                var students = from s in ctx.Students group s by s.StudentName into studentByName select studentByName;

                foreach(var groupItem in students)
                {
                    Console.WriteLine(groupItem.Key);
                    foreach(var stud in groupItem)
                    {
                        Console.WriteLine(stud.StudentName);
                    }
                }

                //---Lambda vs LINQ
                var students1 = ctx.Students.GroupBy(s => s.StudentName);

                foreach (var groupItem in students1)
                {
                    Console.WriteLine(groupItem.Key);
                    foreach (var stud in groupItem)
                    {
                        Console.WriteLine(stud.StudentName);
                    }
                }

                //---OrderBy
                var students2 = from s in ctx.Students orderby s.StudentName ascending select s;

                //---OrderBy vs Lambda
                var students3 = ctx.Students.OrderBy(s => s.StudentName).ToList();

                var students4 = ctx.Students.OrderByDescending(s => s.StudentName).ToList();
            }
        }
    }
}
