﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCodeFirstApplication
{
    public class FptSchoolContext : DbContext
    {
        //base() -> Mac dinh ket noi den localDB
        //base("ten cua db") -> db local Sql Express co san
        //base(name="ten cua connectionString")
        public FptSchoolContext() : base("name=FptDBCodeFirst")
        {
            Database.SetInitializer<FptSchoolContext>(new FptSchoolDbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new StudentConfiguration());
            modelBuilder.Entity<Teacher>().ToTable("TeacherTable");
            modelBuilder.Entity<Teacher>().MapToStoredProcedures();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<StudentAddress> StudentAddresses { get; set; }
    }
}
