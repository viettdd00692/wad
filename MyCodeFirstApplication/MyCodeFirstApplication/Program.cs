﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCodeFirstApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var ctx = new FptSchoolContext())
            {
                var student = new Student() { StudentName = "Duc Viet" };
                ctx.Students.Add(student);
                ctx.SaveChanges();
            }
            Console.WriteLine("Create databse code first successful");
            Console.ReadLine();
        }
    }
}
